#include "stm32f0xx.h"
#include "lcd_stm32f0.h"
#define TC74ADDR 0b1001000

uint8_t receive = 0;
uint8_t send = 0;

void main(void);
void init_iic(void);
void init_leds(void);
void ResetClockTo48Mhz(void);
void LCD_NumDisplay(uint8_t);
void lcd_init(void);
void lcd_display(void);
void InitButtons(void);
void InitExternalInterrupts(void);
void init_Usart1(void);

void main(void)
{
	ResetClockTo48Mhz();

	init_leds();
	init_iic();
	lcd_init();

	InitButtons();
	InitExternalInterrupts();

	init_Usart1();

	while(1)
	{

	}
}
//Master Receiver Mode, Read 1 Byte, Read temperature from TC74 sensor on PF6 and PF7

void init_iic(void)
{
	// enable clock to port F
	RCC->AHBENR |= RCC_AHBENR_GPIOFEN;
	// there is a risk that the slave is sitting in the middle
	// of a transfer when we reset the master.
	// the following block of code simply toggles the clock
	// line 20 times to ensure that the slave gets a chance to
	// clock out its data.
	// set clock line to open drain, output
	GPIOF->OTYPER |= GPIO_OTYPER_OT_6; // open drain
	GPIOF->MODER |= GPIO_MODER_MODER6_0; // GP output
	for (uint32_t loop_counter = 0; loop_counter < 20; loop_counter++)
	{
		for (volatile uint32_t delay = 0; delay < 10; delay++);
		GPIOF->BSRR |= GPIO_BSRR_BR_6; // set clock low
		for (volatile uint32_t delay = 0; delay < 10; delay++);
		GPIOF->BSRR |= GPIO_BSRR_BS_6; // set clock high
	}
	// set SCLK (PF6) to alternate function, open drain
	GPIOF->MODER &= ~GPIO_MODER_MODER6; // reset the MODER bits
	GPIOF->MODER |= GPIO_MODER_MODER6_1;
	GPIOF->OTYPER |= GPIO_OTYPER_OT_6;
	// set SDA (PF7) to alternate function, open drain
	GPIOF->MODER |= GPIO_MODER_MODER7_1;
	GPIOF->OTYPER |= GPIO_OTYPER_OT_7;
	// PF6 and PF7 only have 1 alternate function, so it's not
	// necessary to map them
	// enable clock to I2C2
	RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;
	// disable the peripheral
	I2C2->CR1 &= ~I2C_CR1_PE;
	// configure timing in PRESC, SCLDEL, SDADEL in TIMINGR
	I2C2->TIMINGR |= (0xC7 << 0); // SCLL
	I2C2->TIMINGR |= (0xC3 << 8); // SCLH
	I2C2->TIMINGR |= (0x02 << 16); // SDADEL
	I2C2->TIMINGR |= (0x04 << 20); // SCLDEL
	I2C2->TIMINGR |= (0x0B << 28);// PRESC
	// enable I2C
	I2C2->CR1 |= I2C_CR1_PE;
}

void init_leds(void)
{
RCC->AHBENR |= RCC_AHBENR_GPIOBEN; //enable clock for LEDs
GPIOB->MODER |= GPIO_MODER_MODER0_0; //set PB0 to output
GPIOB->MODER |= GPIO_MODER_MODER1_0; //set PB1 to output
GPIOB->MODER |= GPIO_MODER_MODER2_0; //set PB2 to output
GPIOB->MODER |= GPIO_MODER_MODER3_0; //set PB3 to output
GPIOB->MODER |= GPIO_MODER_MODER4_0; //set PB4 to output
GPIOB->MODER |= GPIO_MODER_MODER5_0; //set PB5 to output
GPIOB->MODER |= GPIO_MODER_MODER6_0; //set PB6 to output
GPIOB->MODER |= GPIO_MODER_MODER7_0; //set PB7 to output
}

void ResetClockTo48Mhz(void)									   //COMPULSORY
{																   //COMPULSORY
	if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL)			   //COMPULSORY
	{															   //COMPULSORY
		RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW);					   //COMPULSORY
		while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI);	   //COMPULSORY
	}															   //COMPULSORY

	RCC->CR &= (uint32_t)(~RCC_CR_PLLON);						   //COMPULSORY
	while ((RCC->CR & RCC_CR_PLLRDY) != 0);						   //COMPULSORY
	RCC->CFGR = ((RCC->CFGR & (~0x003C0000)) | 0x00280000);		   //COMPULSORY
	RCC->CR |= RCC_CR_PLLON;									   //COMPULSORY
	while ((RCC->CR & RCC_CR_PLLRDY) == 0);						   //COMPULSORY
	RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_PLL);					   //COMPULSORY
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);		   //COMPULSORY
}

void lcd_display(void)
{

	char numArray[16];
	lcd_command(LCD_CURSOR_HOME);
	sprintf(numArray, "%d", send);
	lcd_string(numArray);
}

void InitButtons(void)  //2- clock to GPIOA, pull-up resistor on PA3
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN; 	//enable clock for Buttons
	GPIOA->MODER &= ~GPIO_MODER_MODER3; 	//explicitly set PA3 to input
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR3_0;	//switch on pull-up resistor
}

void InitExternalInterrupts(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGCOMPEN; 	  //1	//enable clock for sysconfig

	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI3_PA; //1	//route PA to EXTI3
	EXTI->IMR |= EXTI_IMR_MR3;  				  //1	//unmask the interrupt on PA3
	EXTI->FTSR |= EXTI_FTSR_TR3;				  //1 	//falling-edge trigger

	NVIC_EnableIRQ(EXTI2_3_IRQn); 				  //1 	//enable the EXTI2_3 interrupt on
														//NVIC
}

// INTERRUPT HANDLERS --------------------------------------------------------|
void EXTI2_3_IRQHandler(void)			//1
{
	//clear the interrupt pending bit by writing to it
	EXTI->PR |= EXTI_PR_PR3;			//1

	I2C2->CR2 |= (TC74ADDR << 1); //set slave address in SADD
			I2C2->CR2 |= (1 << 16); // set NBYTES to 1
			// send start and address with read byte.
			// indicate we are going to be doing a read
			I2C2->CR2 |= I2C_CR2_RD_WRN;
			I2C2->CR2 |= I2C_CR2_START;
			// wait for ACK (start bit reset)
			// wait for RX flag
			while ((I2C2->ISR & I2C_ISR_RXNE) == 0);
			// clock in a byte and write to LEDs
			send = I2C2 ->RXDR;
			// STOP condition
			I2C2->CR2 |= I2C_CR2_STOP;


			GPIOB -> ODR = send; //write to value to variable send
			USART1 -> TDR = send; //transfer value send
			while ((USART1->ISR & USART_ISR_TC) == 0);

			lcd_display();
}

void init_Usart1(void)
{
	// Clock to GPIOA
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	// PA9 and PA10 to AF
	GPIOA->MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
	// Remap to correct AF: PA9 to AF1, PA10 to AF1
	GPIOA->AFR[1] |= (1 << 4) | (1 << 8);

	// Clock to USART1
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

	// Set baud rate to 115200
	// USART1->BRR = 48000000/115200
	USART1->BRR = 0x1A1;

	// Set word length to 8 bits
	USART1->CR1 &= ~USART_CR1_M;

	// Set to RXNE interrupt generation
	USART1->CR1 |= USART_CR1_RXNEIE;

	// Set to transmit and receive
	USART1->CR1 |= USART_CR1_TE | USART_CR1_RE;

	// Enable USART
	USART1->CR1 |= USART_CR1_UE;

	NVIC_EnableIRQ(USART1_IRQn); //enable interupt

}

void USART1_IRQHandler(void)
{
	receive = USART1->RDR;
	char diffArray[16];
	lcd_command(LCD_GOTO_LINE_2);
	sprintf(diffArray, "%d", receive);
	lcd_string(diffArray);
}
